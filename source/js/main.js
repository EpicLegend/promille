//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= library/wow.js


$(document).ready(function () {


	/* анимация блоков */
	new WOW().init({
		mobile: false
	});

	/* блики кнопок */
	setInterval(function () {
		$(".btn__flare_infinite").toggleClass("active");
	}, 2000);

	$(window).on("scroll", function () {

		/* кнопка вверх */
		if ( $(window).scrollTop() > 100 ) {
			$(".btn_to_top").addClass("active");
		} else {
			$(".btn_to_top").removeClass("active");
		}
	});


	$('.btn_to_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });




});
